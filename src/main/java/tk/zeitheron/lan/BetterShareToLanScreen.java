package tk.zeitheron.lan;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.HTTPUtil;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.GameType;
import net.minecraft.world.dimension.DimensionType;
import tk.zeitheron.hammerlib.util.java.ReflectionUtil;

public class BetterShareToLanScreen
		extends Screen
{
	private final Screen lastScreen;
	private Button allowCheatsButton, allowPVPButton, onlineModeButton;
	private Button gameModeButton;
	private String gameMode = "survival";
	private boolean allowCheats, allowPVP = true, onlineMode = true;
	private TextFieldWidget portField, maxPlayersField, motdField;

	public static final int MAX_PORT = 65_535;
	
	public BetterShareToLanScreen(Screen lastScreen)
	{
		super(new TranslationTextComponent("lanServer.title"));
		this.lastScreen = lastScreen;
	}

	@Override
	protected void init()
	{
		this.addButton(new Button(this.width / 2 - 155, this.height - 28, 150, 20, I18n.format("lanServer.start"), button ->
		{
			this.minecraft.displayGuiScreen(null);
			int port = HTTPUtil.getSuitableLanPort();
			TranslationTextComponent msg;
			IntegratedServer is = this.minecraft.getIntegratedServer();
			if(is.shareToLAN(GameType.getByName(this.gameMode), this.allowCheats, port))
			{
				is.setAllowPvp(this.allowPVP);
				is.setOnlineMode(this.onlineMode);
				is.setMOTD(motdField.getText());

				try
				{
					ReflectionUtil.setFinalField(PlayerList.class.getDeclaredFields()[17], is.getPlayerList(), LAN.LANConfigs.maxPlayers);
				} catch(ReflectiveOperationException e)
				{
					e.printStackTrace();
				}

				msg = new TranslationTextComponent("commands.publish.started", port);

				String on = I18n.format("options.on");
				String off = I18n.format("options.off");

				this.minecraft.ingameGUI.getChatGUI().printChatMessage(new StringTextComponent(I18n.format("commands.lan:publish.info", onlineMode ? on : off, allowPVP ? on : off, port, motdField.getText(), is.getPlayerList().getMaxPlayers())));
			} else msg = new TranslationTextComponent("commands.publish.failed");
			this.minecraft.ingameGUI.getChatGUI().printChatMessage(msg);
			this.minecraft.func_230150_b_();
		}));

		this.addButton(new Button(this.width / 2 + 5, this.height - 28, 150, 20, I18n.format("gui.cancel"), button ->
		{
			this.minecraft.displayGuiScreen(this.lastScreen);
		}));

		this.gameModeButton = this.addButton(new Button(this.width / 2 - 155, 100, 150, 20, I18n.format("selectWorld.gameMode"), button ->
		{
			if("spectator".equals(this.gameMode))
			{
				this.gameMode = "creative";
			} else if("creative".equals(this.gameMode))
			{
				this.gameMode = "adventure";
			} else if("adventure".equals(this.gameMode))
			{
				this.gameMode = "survival";
			} else
			{
				this.gameMode = "spectator";
			}

			this.updateDisplayNames();
		}));

		this.allowCheatsButton = this.addButton(new Button(this.width / 2 + 5, 100, 150, 20, I18n.format("selectWorld.allowCommands"), button ->
		{
			this.allowCheats = !this.allowCheats;
			this.updateDisplayNames();
		}));

		this.allowPVPButton = this.addButton(new Button(this.width / 2 + 5, 150, 150, 20, I18n.format("selectWorld.allowPvP"), button ->
		{
			this.allowPVP = !this.allowPVP;
			this.updateDisplayNames();
		}));

		this.onlineModeButton = this.addButton(new Button(this.width / 2 - 155, 150, 150, 20, I18n.format("selectWorld.onlineMode"), button ->
		{
			this.onlineMode = !this.onlineMode;
			this.updateDisplayNames();
		}));

		String text = portField != null ? portField.getText() : Integer.toString(LAN.LANConfigs.port);
		portField = new TextFieldWidget(this.font, width / 2 - 124, 124, 50, 20, "Port");
		portField.setTextColor(0xffffffff);
		portField.setDisabledTextColour(0xffffffff);
		portField.setMaxStringLength(5);
		portField.setText(text);
		portField.setValidator(str ->
		{
			try
			{
				if(str.isEmpty()) return true;
				Integer.parseInt(str);
				return true;
			} catch(Throwable e)
			{
				return false;
			}
		});
		portField.setResponder(str ->
		{
			try
			{
				int parsed = Math.max(0, Math.min(Integer.parseInt(str), MAX_PORT));
				String str2 = Integer.toString(parsed);
				if(!str2.equals(str)) portField.setText(str2);
				LAN.LANConfigs.port = parsed;
				LAN.LANConfigs.save();
			} catch(Throwable e)
			{
				portField.setText("0");
			}
		});
		this.children.add(this.portField);


		text = maxPlayersField != null ? maxPlayersField.getText() : Integer.toString(LAN.LANConfigs.maxPlayers);
		maxPlayersField = new TextFieldWidget(this.font, width / 2 - 75, 185, 150, 20, "Max Players");
		maxPlayersField.setTextColor(0xffffffff);
		maxPlayersField.setDisabledTextColour(0xffffffff);
		maxPlayersField.setMaxStringLength(5);
		maxPlayersField.setText(text);
		maxPlayersField.setValidator(str ->
		{
			try
			{
				if(str.isEmpty()) return true;
				Integer.parseInt(str);
				return true;
			} catch(Throwable e)
			{
				return false;
			}
		});
		maxPlayersField.setResponder(str ->
		{
			try
			{
				int parsed = Math.max(0, Integer.parseInt(str));
				String str2 = Integer.toString(parsed);
				if(!str2.equals(str)) maxPlayersField.setText(str2);
				LAN.LANConfigs.maxPlayers = parsed;
				LAN.LANConfigs.save();
			} catch(Throwable e)
			{
				maxPlayersField.setText("0");
			}
		});
		this.children.add(this.maxPlayersField);


		String defaultMOTD = minecraft.getIntegratedServer().getServerOwner() + " - " + minecraft.getIntegratedServer().getWorld(DimensionType.OVERWORLD).getWorldInfo().getWorldName();

		text = motdField != null ? motdField.getText() : defaultMOTD;
		motdField = new TextFieldWidget(this.font, width / 2 + 36, 124, 118, 20, "MOTD");
		motdField.setTextColor(0xffffffff);
		motdField.setDisabledTextColour(0xffffffff);
		motdField.setMaxStringLength(32);
		motdField.setResponder(str ->
		{
			LAN.LANConfigs.motd = str.compareTo(defaultMOTD) == 0 ? "" : str;
			LAN.LANConfigs.save();
		});
		motdField.setText(text);
		this.children.add(this.motdField);


		this.updateDisplayNames();
	}

	private void updateDisplayNames()
	{
		this.gameModeButton.setMessage(I18n.format("selectWorld.gameMode") + ": " + I18n.format("selectWorld.gameMode." + this.gameMode));
		this.allowCheatsButton.setMessage(I18n.format("selectWorld.allowCommands") + ' ' + I18n.format(this.allowCheats ? "options.on" : "options.off"));
		this.allowPVPButton.setMessage(I18n.format("selectWorld.allowPvP") + ' ' + I18n.format(this.allowPVP ? "options.on" : "options.off"));
		this.onlineModeButton.setMessage(I18n.format("selectWorld.onlineMode") + ' ' + I18n.format(this.onlineMode ? "options.on" : "options.off"));
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTime)
	{
		this.renderBackground();
		this.drawCenteredString(this.font, this.title.getFormattedText(), this.width / 2, 50, 0xFFFFFF);
		this.drawCenteredString(this.font, I18n.format("lanServer.otherPlayers"), this.width / 2, 82, 0xFFFFFF);
		super.render(mouseX, mouseY, partialTime);

		font.drawString(I18n.format("selectWorld.port"), gameModeButton.x, portField.y + 5, 0xffffffff);
		font.drawString(I18n.format("selectWorld.motd"), allowCheatsButton.x, motdField.y + 5, 0xffffffff);
		String text = I18n.format("selectWorld.maxPlayers");
		font.drawString(text, (width - font.getStringWidth(text)) / 2, 173, 0xffffffff);
		portField.render(mouseX, mouseY, partialTime);
		motdField.render(mouseX, mouseY, partialTime);
		maxPlayersField.render(mouseX, mouseY, partialTime);
	}

	@Override
	public boolean keyPressed(int p_keyPressed_1_, int p_keyPressed_2_, int p_keyPressed_3_)
	{
		return portField.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_) || portField.canWrite()
				|| motdField.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_) || motdField.canWrite()
				|| maxPlayersField.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_) || maxPlayersField.canWrite()
				|| super.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
	}
}