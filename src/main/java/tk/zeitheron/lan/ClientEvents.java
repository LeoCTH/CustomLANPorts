package tk.zeitheron.lan;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ShareToLanScreen;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import tk.zeitheron.hammerlib.event.client.GetSuitableLanPortEvent;
import tk.zeitheron.hammerlib.util.java.ReflectionUtil;

import java.lang.reflect.Field;

@OnlyIn(Dist.CLIENT)
@Mod.EventBusSubscriber(Dist.CLIENT)
public class ClientEvents
{
	@SubscribeEvent
	public static void selectPort(GetSuitableLanPortEvent e)
	{
		if(LAN.LANConfigs.port != 0) e.setNewPort(LAN.LANConfigs.port);
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public static void guiOpen(GuiOpenEvent e)
	{
		if(e.getGui() instanceof ShareToLanScreen)
		{
			Field f = ReflectionUtil.lookupField(ShareToLanScreen.class, Screen.class);
			f.setAccessible(true);
			try
			{
				Screen last = Screen.class.cast(f.get(e.getGui()));
				e.setGui(new BetterShareToLanScreen(last));
			} catch(IllegalAccessException ex)
			{
				ex.printStackTrace();
			}
		}
	}
}